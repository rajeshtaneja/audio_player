import 'package:get/get.dart';
import 'package:universal_html/controller.dart';

class AlbumsListPageController extends GetxController {
  final site = 'https://songspk.blog/hindi-mp3-songs.html';
  final controller = WindowController();
  var albums = [].obs;

  @override
  void onInit() {
    super.onInit();
    getAlbumns();
  }

  Future<void> getAlbumns() async {
    await controller.openHttp(
      uri: Uri.parse(site),
    );
    var albumList = controller.window!.document
        .querySelectorAll("section#about > div.container > div.row > a");

    albumList.forEach((album) {
      var albumName = album.querySelector('div > p');
      var albumImageLink = album.querySelector('div > img');

      if ((albumName != null) && (albumImageLink != null)) {
        albums.add({
          'albumn_name': albumName.text,
          'album_image_link': albumImageLink.getAttribute('src'),
          'album_page_link': album.getAttribute('href'),
        });
      }
    });
  }
}
