import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/albums_list_page_controller.dart';

class HomePage extends StatelessWidget {
  final String title;

  final AlbumsListPageController controller =
      Get.put(AlbumsListPageController());

  HomePage({required this.title});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Scaffold(
        body: SafeArea(
          child: GetBuilder<AlbumsListPageController>(builder: (_controller) {
            if (_controller.albums.length == 0) {
              return Text('Loading');
            } else if (_controller.albums.isEmpty) {
              return Text('Empty List');
            } else {
              return ListView.builder(
                itemBuilder: (BuildContext ctx, int index) {
                  return Image.network(
                      _controller.albums.value[index]['album_image_link']);
                },
                itemCount: _controller.albums.value.length,
              );
            }
          }),
        ),
      ),
    );
  }
}
