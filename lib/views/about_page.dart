import 'package:flutter/material.dart';

// import '../controllers/songs_list_controller.dart';

class AboutPage extends StatelessWidget {
  // final SongsListController controller = Get.put(SongsListController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GetX'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'GetX is an extra-light and powerful solution for Flutter. It combines high performance state management, intelligent dependency injection, and route management in a quick and practical way.',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
